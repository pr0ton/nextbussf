var webpack = require('webpack');
var BowerWebpackPlugin = require("bower-webpack-plugin");

module.exports = {
  entry: {
    main: "./js/main.js"
  },
  output: {
    path: __dirname,
    filename: "public/javascript/bundle.js"
  },
  module: {
    loaders: [
      {
        test:   /\.css$/,
        loader: "style-loader!css-loader"
      },
      {test: /\.(woff|svg|ttf|eot)([\?]?.*)$/, loader: "file-loader?name=[name].[ext]"}
    ]
  },
  plugins: [
    new BowerWebpackPlugin({
    modulesDirectories: ["bower_components"],
    manifestFiles:      "bower.json",
    includes:           /.*/,
    excludes:           /.*\.less/
  })
  ],
};
