define(function(require) {
  var geo = require('./geolocator.js'),
      dataloader = require('./dataloader.js');


  geo({
    navigator: navigator,
    success: function acquiredGeo(position) {
      dataloader.loadGeo(position);
    },
    error: function failedToAcquireGeo() {
      console.log("no geo");
    }
  });

  window.jQuery = require('jquery');

  require("bootstrap")
  require('../css/main.css');

});

