define(function dataLoader() {
  var $ = require('jquery');
  return {
    loadGeo: function(position) {
      var coords = position.coords;
      $.ajax('/api/v1/closest.json', 
        {
          method: 'post',
          data: {
            lon: coords.longitude,
            lat: coords.latitude
          },
          success: function onDataLoad(data, textStatus) {
            // show success message
            $("#message-box").html(data);
          },
          error: function onDataError(xhr, textStatus) {
            // show error message
            console.log("such failure");
          }
        }
      );
    }
  };
});
