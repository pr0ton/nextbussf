define(function geoLocator() {
  return function(params) {
    var geo = params.navigator || navigator;
    if ("geolocation" in geo) {
      /* geolocation is available */
      geo.geolocation.getCurrentPosition(function(position) {
         var success = params.success;
         if (success && typeof success === 'function') {
           success(position);
         } 
      });
    } else {
      var error = params.error;
      if (error && typeof error === 'function') {
        error();
      }
    }
   }
});
