package controllers

import controllers.api.ApiDataFetcher
import controllers.parsers._
import play.api.mvc._
import scala.concurrent.Future
import scala.util.Try


object Application extends Controller {
  import Coord._
  def index = Action {
    Ok(views.html.Application.index("SF Bus predictions"))
  }

  implicit val context = scala.concurrent.ExecutionContext.Implicits.global
  val apiDataFetcher = new ApiDataFetcher
  lazy val apiDataFuture = apiDataFetcher.fetchData
  def closest = Action.async { request =>
    val paramMap = request.body.asFormUrlEncoded.getOrElse { Map.empty }

    def param(p: String): Option[Double] =
      paramMap.get(p)
        .flatMap { _.headOption }
        .flatMap { v => Try { v.toDouble }.toOption }

    (param("lat"), param("lon")) match {
      case (Some(lat), Some(lon)) =>
        apiDataFuture.flatMap { apiData =>
          val point = Coord(lat, lon)
          val results =
            apiData.stopMap.values.map { stop =>
              (stop, point.distanceTo(stop.point))
            }.toSeq.sortBy(_._2).filter(_._2 < 0.3)

          apiDataFetcher.predictionsForMultiStops(
            results.flatMap { case (s, dist) =>
              apiData.getAllRoutes(s.stopId).map { route =>
                s -> route
              }
            }
          ).map { predictions =>
            Ok(views.html.Application.predictions.render(predictions))
          }
        }
      case _ =>
        Future(BadRequest)
    }
  }
}
