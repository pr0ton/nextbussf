package controllers.parsers

case class Coord(lat: Double, lon: Double)

object Coord {

  val EarthRadius = 3958.75; // miles (or 6371.0 kilometers)
  implicit class CoordDistance(val p1: Coord) extends AnyVal {
    def distanceTo(p2: Coord): Double = {
      val dLat = Math.toRadians(p1.lat - p2.lat)
      val dLng = Math.toRadians(p1.lon - p2.lon)
      val sindLat = Math.sin(dLat / 2)
      val sindLng = Math.sin(dLng / 2)
      val a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(p2.lat)) * Math.cos(Math.toRadians(p1.lat))
      val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
      EarthRadius * c
    }
  }
}
