package controllers.parsers

case class Event(minutes: String)
case class StopPrediction(routeTitle: String, stopTitle: String, direction: String, events: Seq[Event])

class MultiPredictionParser extends Parser[Seq[StopPrediction]] {
  override def apply(input: String) = {
    val xml = toXml(input)
    (xml \ "predictions").map { predictions =>
      val routeTitle = predictions \ "@routeTitle" text
      val stopTitle = predictions \ "@stopTitle" text

      (predictions \ "direction").headOption.map { dir =>
        val direction = dir \ "@title" text

        val events = (dir \ "prediction").map { p =>
          Event(p \ "@minutes" text)
        }
        StopPrediction(routeTitle, stopTitle, direction, events.take(3))
      }
    }.flatten
  }
}
