package controllers.parsers

case class Route(tag: String, title: String)

class RouteParser extends Parser[Map[String, Route]] {
  override def apply(input: String): Map[String, Route] = {
    val xml = toXml(input)
    (xml \\ "route").map { route =>
      val tag = route \ "@tag" text

      tag -> Route(tag, route \ "@title" text)
    }.toMap
  }
}
