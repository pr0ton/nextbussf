package controllers.parsers

import java.io.ByteArrayInputStream

import scala.xml.XML

abstract class Parser[T] {
  def apply(input: String): T
  def toXml(input: String) =
    XML.load(new ByteArrayInputStream(input.getBytes))
}
