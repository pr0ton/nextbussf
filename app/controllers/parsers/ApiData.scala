package controllers.parsers

case class ApiData(routeMap: Map[String, Route],
                   stopToRoutes: Map[String, Seq[String]],
                   stopMap: Map[String, RouteStop]) {
  def getAllRoutes(stopId: String): Seq[Route] = {
    stopToRoutes.get(stopId).getOrElse { Seq.empty }.flatMap { routeId =>
      routeMap.get(routeId).toSeq
    }
  }
}
