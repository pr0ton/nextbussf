package controllers.parsers


case class RouteStop(stopId: String, lon: Double, lat: Double, title: String, tag: String) {
  val point = Coord(lat, lon)
}

class RouteStopParser extends Parser[Seq[RouteStop]] {
  override def apply(input: String) = {
    val xml = toXml(input)
    (xml \\ "stop").map { stop =>
      def a(attribute: String): Option[String] =
        Option(stop \ s"@$attribute" text).filter { _.length > 0 }

      def ad(attribute: String): Option[Double] =
        a(attribute).map { _.toString.toDouble }

      (a("stopId"), ad("lon"), ad("lat"), a("title"), a("tag")) match {
        case (Some(stopId), Some(lon), Some(lat), Some(title), Some(tag)) =>
          Some(RouteStop(stopId, lon, lat, title, tag))
        case _ => None
      }
    }.flatten
  }
}
