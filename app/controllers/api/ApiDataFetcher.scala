package controllers.api

import controllers.parsers._

import play.api.Play.current
import play.Logger
import play.api.libs.ws.WS

import scala.concurrent.{ExecutionContext, Future}

class ApiDataFetcher(implicit ctx: ExecutionContext) {

  val Agency = "sf-muni"
  def mkRequest(command: String, customParams: (String, String)*) = {
    val queryParams = Seq("command" -> command) ++ Seq(customParams: _*)
    Logger.debug(s"Querying cmd=$command with params=$customParams")
    WS.url("http://webservices.nextbus.com/service/publicXMLFeed").withQueryString(
      queryParams: _*
    ).get
  }



  val routeParser = new RouteParser()
  val routeStopParser = new RouteStopParser()

  def fetchData: Future[ApiData] = {
    // Fetches from the API and stores a map of (TagId -> Route(tagId, RouteName))
    val routesMapFuture = {
      mkRequest("routeList", "a" -> Agency).map { r => routeParser(r.body) }
    }

    // Fetches from the API and stores a map of (routeTag -> Stop(stopId, lat, long ...)
    routesMapFuture.flatMap { routesMap =>
      Future.sequence(routesMap.keys.map { routeTagId =>
        mkRequest("routeConfig",
          "a" -> Agency,
          "r" -> routeTagId
        ).map { r => routeStopParser(r.body) }.map { stops =>
          routeTagId -> stops
        }
      }).map { items =>
        val routeConfigMap = items.toMap
        // Takes Map of (routeTagId -> Seq[Stop]) and produces some other datastructures
        val stopId2Routes =
          routeConfigMap.iterator.toSeq.flatMap { case (routeTagId, stops) =>
            stops.map { stop =>
              stop.stopId -> routeTagId
            }.groupBy { _._1 }.mapValues { vals =>
              vals.map { _._2 }
            }
          }.toMap

        val stopId2Stop =
          routeConfigMap.values.flatten.map { stop =>
            stop.stopId -> stop
          }
        ApiData(routesMap, stopId2Routes, stopId2Stop.toMap)
      }
    }
  }


  val multipredictionParser = new MultiPredictionParser()
  def predictionsForMultiStops(query: Seq[(RouteStop, Route)]): Future[Seq[StopPrediction]] = {
    Future.sequence(
      for (stops <- query.grouped(1).toList) yield {
        val queryStops = stops.map { case (routeStop, route) =>
          "stops" -> s"${route.tag}|${routeStop.tag}"
        } ++ Seq("a" -> Agency)
        mkRequest(
          "predictionsForMultiStops",
          queryStops: _*
        ).map { r =>
          multipredictionParser(r.body)
        }
      }
    ).map { _.flatten }
  }

}
