#!/usr/bin/env bash

SNAPSHOT=nextbus-sf-1.0-SNAPSHOT
DIST_NAME=$SNAPSHOT.zip
LOCAL_BINARY=$(pwd)/target/universal/$DIST_NAME

# TODO: Can automate this to use "gcloud compute instances list" 
INSTANCE=nextbus-frontend-mkb0
ZONE=us-central1-f
