#!/usr/bin/env bash

source ./scripts/constants.sh
echo "Stopping server ..."
gcloud compute ssh root@$INSTANCE --zone $ZONE --command "cat /usr/webserver/nextbus-sf-1.0-SNAPSHOT/RUNNING_PID | xargs kill -s SIGTERM"
