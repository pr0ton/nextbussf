#!/usr/bin/env bash
source ./scripts/constants.sh

echo "Building frontend ..."
webpack --optimize-minimize --optimize-dedupe
echo "Building binary ..."
activator dist
echo "Binary is $LOCAL_BINARY"

# gcloud compute ssh root@$INSTANCE --zone $ZONE --command "apt-get update"
# gcloud compute ssh root@$INSTANCE --zone $ZONE --command "apt-get install -y unzip openjdk-7-jre-headless"
./scripts/stop.sh

echo "Cleaning up webserver"
gcloud compute ssh root@$INSTANCE --zone $ZONE --command "rm -rf /usr/webserver/"
gcloud compute ssh root@$INSTANCE --zone $ZONE --command "mkdir -p /usr/webserver/"

echo "Uploading binary ..."
gcloud compute copy-files $LOCAL_BINARY root@$INSTANCE:/usr/webserver/ --zone $ZONE

echo "Unzipping binary ..."
gcloud compute ssh root@$INSTANCE --zone $ZONE --command "cd /usr/webserver/ && unzip $DIST_NAME"

./scripts/start.sh
