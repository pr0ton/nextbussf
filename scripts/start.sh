#!/usr/bin/env bash

source ./scripts/constants.sh

echo "Starting server ..."
gcloud compute ssh root@$INSTANCE --zone $ZONE --command "/usr/webserver/$SNAPSHOT/bin/nextbus-sf -Dhttp.port=80 &"
